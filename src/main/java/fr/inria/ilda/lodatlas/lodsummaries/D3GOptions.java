/*
 * LODSummaries
 * Copyright (C) 2018  INRIA Emmanuel Pietriga <emmanuel.pietriga@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.inria.ilda.lodatlas.lodsummaries;

import java.util.ArrayList;
import java.util.List;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

public class D3GOptions {

	@Option(name = "-g", aliases = { "--generator" }, usage = "Which generator: {fd, heb}")
	public String generator = "foo";

	@Option(name = "-f", aliases = { "--input-file" }, usage = "Summary triples file")
	public String path_to_summary_triples = null;

	@Option(name = "-o", aliases = { "--output-file" }, usage = "JSON vis file")
	public String path_to_output = D3JSONGenerator.DEFAULT_OUTPUT_FILE;

	@Argument
	List<String> arguments = new ArrayList<String>();

	public boolean standalone = true; // not a CLI option

}
