/*
 * LODSummaries
 * Copyright (C) 2018  INRIA Emmanuel Pietriga <emmanuel.pietriga@inria.fr>
 * Copyright (C) 2018  INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.inria.ilda.lodatlas.lodsummaries;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class D3JSONGenerator {

	static final String NS_RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	static final String _type = "type";
	static final String NS_RDFS = "http://www.w3.org/2000/01/rdf-schema#";
	static final String _subClassOf = "subClassOf";
	static final String _subPropertyOf = "subPropertyOf";
	static final String _domain = "domain";
	static final String _range = "range";
	static final String NS_OWL = "http://www.w3.org/2002/07/owl#";
	static final String _sameAs = "sameAs";
	static final String _equivalentClass = "equivalentClass";

	// fake URI for triples containing relative URIs
	static final String BASE_URI = "http://ilda.saclay.inria.fr/";
	static final String INPUT_FORMAT_NTRIPLES = "N-TRIPLES";
	static final String DEFAULT_OUTPUT_FILE = "output.json";

	static final String EMPTY_STRING = "";

	void writeJsontoFile(JsonObject json, File ouputFile) {
		// write the JSON and save it to a file
		JsonWriter jw = null;
		try {
			jw = Json.createWriter(new FileOutputStream(ouputFile));
			System.out.println("--- Writing output to:\n" + ouputFile.getAbsolutePath());
			jw.writeObject(json);
		} catch (FileNotFoundException fex) {
			fex.printStackTrace();
		} finally {
			if (jw != null) {
				jw.close();
			}
		}
	}

	public static void main(String[] args) {
		D3GOptions options = new D3GOptions();
		CmdLineParser parser = new CmdLineParser(options);
		try {
			parser.parseArgument(args);
		} catch (CmdLineException ex) {
			System.err.println(ex.getMessage());
			parser.printUsage(System.err);
			return;
		}
		if (options.generator.equals("fd")) {
			new D3JSONGeneratorFD(options);
		} else if (options.generator.equals("heb")) {
			new D3JSONGeneratorHEB(options);
		} else {
			parser.printUsage(System.err);
		}
	}

}
