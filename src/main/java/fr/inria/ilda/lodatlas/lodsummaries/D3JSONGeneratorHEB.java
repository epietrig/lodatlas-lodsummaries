/*
 * LODSummaries
 * Copyright (C) 2018  INRIA Emmanuel Pietriga <emmanuel.pietriga@inria.fr>
 * Copyright (C) 2018  INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.inria.ilda.lodatlas.lodsummaries;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.NsIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.FileManager;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class D3JSONGeneratorHEB extends D3JSONGenerator {

	static final String[] IGNORED_NAMESPACES = { NS_RDF, NS_RDFS };

	HashMap<String, Integer> ns2colorgroup = new HashMap();
	Vector<HEBNode> nodes = new Vector();
	Vector<HEBLink> links = new Vector();

	Model summary;

	public D3JSONGeneratorHEB(D3GOptions options) {
		generateJSON(options.path_to_summary_triples, options.path_to_output);
		System.exit(0);
	}

	public D3JSONGeneratorHEB() {

	}

	public void reset() {
		nodes.clear();
		links.clear();
		ns2colorgroup.clear();
	}

	public void generateJSON(String pathToSummaryTriples, String outputPath) {
		if (pathToSummaryTriples == null || "".equals(pathToSummaryTriples))
			return;
		loadRDFSummary(pathToSummaryTriples);
		buildViz();
		JsonObject job = generateJSON();
		writeJsontoFile(job, new File(outputPath));
	}

	public void loadRDFSummary(String filePath) {
		System.out.println("--- Loading summary triples from:\n" + filePath);
		// create an empty model
		summary = ModelFactory.createDefaultModel();
		// use the FileManager to find the input file
		InputStream in = FileManager.get().open(filePath);
		if (in == null) {
			throw new IllegalArgumentException("File: " + filePath + " not found");
		}
		// read the N-TRIPLES file
		summary.read(in, BASE_URI, INPUT_FORMAT_NTRIPLES);
		try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void buildViz() {
		NsIterator nsi = summary.listNameSpaces();
		System.out.println("--- Namespaces:");
		int i = 0;

		while (nsi.hasNext()) {
			// assign a unique color group to each namespace
			// (starting with 0, -1 being the group of uncolored namespaces)
			String ns = nsi.nextNs();
			System.out.println(ns);
			if (!isIgnoredNameSpace(ns)) {
				ns2colorgroup.put(ns, new Integer(i++));
			}
		}

		Property rdfType = summary.getProperty(NS_RDF, _type);
		StmtIterator si = summary.listStatements();
		while (si.hasNext()) {
			Statement s = si.nextStatement();
			Property p = s.getPredicate();
			if (p.getNameSpace().equals(NS_RDFS) || p.getNameSpace().equals(NS_OWL)) {
				// RDFS and OWL namespaces
				// XXX will deal with them when we add schema level info,
				// but that won't be in the NL diagram itself
				// can state the subClassOf relationships in nodes' tooltip
			} else if (p.getNameSpace().equals(NS_RDF)) {
				// RDF namespace
				if (p.getLocalName().equals(_type)) {
					Resource object = ((Resource) s.getObject());
					if (isIgnoredNameSpace(object.getNameSpace())) {
						// likely a schema-level statement, declaring a Class or
						// Property
						// ignore it
						continue;
					}
					// add class info for nodes
					String sURI = s.getSubject().getURI();
					int sIndex = HEBNode.indexOf(nodes, sURI);
					if (sIndex == -1) {
						nodes.add(new HEBNode(sURI));
						sIndex = nodes.size() - 1;
					}
					HEBNode n = nodes.elementAt(sIndex);
					n.addClass(object.getLocalName());
					// color node according to its class' namespace
					n.addColorGroup(getColorGroup(((Resource) s.getObject()).getNameSpace()));
				}
			} else {
				// any other property
				String sURI = s.getSubject().getURI();
				String pURI = p.getURI();
				int sIndex = HEBNode.indexOf(nodes, sURI);
				if (sIndex == -1) {
					nodes.add(new HEBNode(sURI));
					sIndex = nodes.size() - 1;
				}
				Resource object = ((Resource) s.getObject());
				String oURI = object.getURI();
				int oIndex;
				if (rdfType != null && object.hasProperty(rdfType)) {
					// if object has rdf:type info, then seek possible prior
					// declaration of that node and use it
					oIndex = HEBNode.indexOf(nodes, oURI);
					if (oIndex == -1) {
						nodes.add(new HEBNode(oURI));
						oIndex = nodes.size() - 1;
					}
				} else {
					// if no class information, look further:
					// - is the object the S(p) of anything?
					// - if it is, then treat it as a unique node
					// - if not, meaning that it is only a T(p),
					// then regardeless of whether this is a single node in the
					// summary or not,
					// duplicate the summary statement's object as many times as
					// there are props
					// pointing to it, as many properties can end up on the same
					// node
					// even if their property's semantics (and thus range) are
					// widely different
					// (making it unlikely that they would point to the same
					// kind of resource)
					if (isSourceInSummary(object)) {
						oIndex = HEBNode.indexOfP(nodes, pURI);
						if (oIndex == -1) {
							nodes.add(new HEBNode(oURI, pURI));
							oIndex = nodes.size() - 1;
						}
					} else {
						// object is only a T(p), duplicate for each property
						// type
						// (but only have one node for a given property type)
						oIndex = HEBNode.indexOfP(nodes, pURI);
						if (oIndex == -1) {
							nodes.add(new HEBNode(oURI, pURI));
							oIndex = nodes.size() - 1;
						}
					}
				}
				links.add(new HEBLink(pURI, sIndex, oIndex, getColorGroup(p.getNameSpace()), p.getLocalName()));
			}
		}
		// XXX reconstruct subclass / subproperty+domain+range info from what's
		// been parsed
		// and add that schema data to the nodes and props where relevant
		summary.close();
	}

	int getColorGroup(String ns) {
		if (ns2colorgroup.containsKey(ns)) {
			return ns2colorgroup.get(ns).intValue();
		} else {
			// ignored namespace (in terms of coloring)
			return -1;
		}
	}

	public JsonObject generateJSON() {
		JsonObjectBuilder job = Json.createObjectBuilder();
		JsonArrayBuilder nab = Json.createArrayBuilder();
		JsonArrayBuilder lab = Json.createArrayBuilder();
		// nodes
		for (int i = 0; i < nodes.size(); i++) {
			HEBNode n = nodes.elementAt(i);
			JsonArrayBuilder lbab = Json.createArrayBuilder();
			for (String lb : n.getLabels()) {
				lbab.add(lb);
			}
			JsonArrayBuilder grab = Json.createArrayBuilder();
			for (int gr : n.getColorGroups()) {
				grab.add(gr);
			}
			nab.add(Json.createObjectBuilder().add("labels", lbab).add("groups", grab).add("prop", n.getPropertyURI()));
		}
		job.add("nodes", nab);
		// links
		for (int i = 0; i < links.size(); i++) {
			HEBLink l = links.elementAt(i);
			lab.add(Json.createObjectBuilder().add("source", l.subjectIdx).add("target", l.objectIdx)
					.add("group", l.colorGroup).add("label", l.label).add("uri", l.uri));
		}
		job.add("links", lab);
		// namespaces / groups
		String[] cgs = new String[ns2colorgroup.size()];
		for (String ns : ns2colorgroup.keySet()) {
			cgs[ns2colorgroup.get(ns).intValue()] = ns;
		}
		JsonArrayBuilder nsab = Json.createArrayBuilder();
		for (String ns : cgs) {
			nsab.add(ns);
		}
		job.add("groups", nsab);

		return job.build();
	}

	static boolean isIgnoredNameSpace(String ns) {
		for (String ins : IGNORED_NAMESPACES) {
			if (ns.equals(ins)) {
				return true;
			}
		}
		return false;
	}

	static boolean isSourceInSummary(Resource r) {
		StmtIterator si = r.listProperties();
		while (si.hasNext()) {
			if (!isIgnoredNameSpace(si.nextStatement().getPredicate().getNameSpace())) {
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) {
		D3GOptions options = new D3GOptions();
		CmdLineParser parser = new CmdLineParser(options);
		try {
			parser.parseArgument(args);
		} catch (CmdLineException ex) {
			System.err.println(ex.getMessage());
			parser.printUsage(System.err);
			return;
		}
		new D3JSONGeneratorHEB(options);
	}

}

class HEBNode {

	String uri;
	String p_uri;
	String[] classURIs;
	String label = D3JSONGenerator.EMPTY_STRING;
	int[] colorGroups;

	HEBNode(String uri) {
		this(uri, null);
	}

	HEBNode(String uri, String pURI) {
		this.uri = uri;
		this.p_uri = pURI;
	}

	void addClass(String cURI) {
		if (classURIs == null) {
			classURIs = new String[] { cURI };
		} else {
			boolean alreadyDeclared = false;
			for (int i = 0; i < classURIs.length; i++) {
				if (classURIs[i].equals(cURI)) {
					alreadyDeclared = true;
					break;
				}
			}
			if (!alreadyDeclared) {
				String[] ncuris = new String[classURIs.length + 1];
				System.arraycopy(classURIs, 0, ncuris, 0, classURIs.length);
				ncuris[classURIs.length] = cURI;
				classURIs = ncuris;
			}
			Arrays.sort(classURIs);
		}
	}

	void addColorGroup(int cg) {
		if (colorGroups == null) {
			colorGroups = new int[] { cg };
		} else {
			boolean alreadyDeclared = false;
			for (int i = 0; i < colorGroups.length; i++) {
				if (colorGroups[i] == cg) {
					alreadyDeclared = true;
					break;
				}
			}
			if (!alreadyDeclared) {
				int[] ncgs = new int[colorGroups.length + 1];
				System.arraycopy(colorGroups, 0, ncgs, 0, colorGroups.length);
				ncgs[colorGroups.length] = cg;
				colorGroups = ncgs;
			}
			Arrays.sort(colorGroups);
		}
	}

	String[] getLabels() {
		if (classURIs == null) {
			return new String[0];
		} else {
			return classURIs;
		}
	}

	int[] getColorGroups() {
		if (colorGroups == null) {
			return new int[0];
		} else {
			return colorGroups;
		}
	}

	// returns the corresponding property URI
	// for untyped nodes (empty string otherwise)
	String getPropertyURI() {
		return (this.p_uri != null) ? this.p_uri : "";
	}

	static int indexOf(Vector<HEBNode> nodes, String uri) {
		for (int i = 0; i < nodes.size(); i++) {
			if (nodes.elementAt(i).uri.equals(uri)) {
				return i;
			}
		}
		return -1;
	}

	static int indexOfP(Vector<HEBNode> nodes, String uri) {
		for (int i = 0; i < nodes.size(); i++) {
			if (nodes.elementAt(i).getPropertyURI().equals(uri)) {
				return i;
			}
		}
		return -1;
	}

}

class HEBLink {

	String uri;
	String label = D3JSONGenerator.EMPTY_STRING;
	int group;
	int subjectIdx, objectIdx;
	// will be null if namespace not found at init time
	int colorGroup;

	HEBLink(String uri, int sIndex, int oIndex, int cg, String label) {
		this.uri = uri;
		this.label = label;
		this.subjectIdx = sIndex;
		this.objectIdx = oIndex;
		this.colorGroup = cg;
	}

}
