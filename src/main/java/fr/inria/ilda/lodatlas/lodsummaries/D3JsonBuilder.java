/*
 * LODSummaries
 * Copyright (C) 2018  INRIA Emmanuel Pietriga <emmanuel.pietriga@inria.fr>
 * Copyright (C) 2018  INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inria.ilda.lodatlas.lodsummaries;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.NsIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.FileManager;
import org.apache.log4j.Logger;

/**
 * This class is generated to merge two json generators. But it is not complete.
 * 
 * @author hande
 *
 */
public class D3JsonBuilder {

	private final static Logger logger = Logger.getLogger(D3JsonBuilder.class.getName());

	public static final String[] IGNORED_NAMESPACES = { D3JSONGenerator.NS_RDF, D3JSONGenerator.NS_RDFS };

	private HashMap<String, Integer> ns2colorgroup = new HashMap<>();

	private final List<Node> nodes = new LinkedList<>();

	private final List<Link> links = new LinkedList<>();

	private Model summary;

	public D3JsonBuilder() {
	}

	public void reset() {
		nodes.clear();
		links.clear();
		ns2colorgroup.clear();
	}

	public void loadRDFSummary(String filePath) {
		reset();
		logger.info("--- Loading summary triples from:\n" + filePath);

		// create an empty model
		summary = ModelFactory.createDefaultModel();

		// use the FileManager to find the input file
		InputStream in = FileManager.get().open(filePath);
		if (in == null) {
			throw new IllegalArgumentException("File: " + filePath + " not found");
		}
		// read the N-TRIPLES file
		summary.read(in, D3JSONGenerator.BASE_URI, D3JSONGenerator.INPUT_FORMAT_NTRIPLES);
		try {
			in.close();
		} catch (IOException e) {
			logger.error("Error while closing rdfsum stream.");
		}

		buildViz();
	}

	private void setColorGroup() {
		logger.info("############### Setting namespace colors ############");
		NsIterator nsi = summary.listNameSpaces();
		int i = 0;

		while (nsi.hasNext()) {
			// assign a unique color group to each namespace
			// (starting with 0, -1 being the group of uncolored namespaces)
			String ns = nsi.nextNs();
			logger.info(ns);
			if (!isIgnoredNameSpace(ns)) {
				ns2colorgroup.put(ns, new Integer(i++));
			}
		}
	}

	private void buildViz() {
		setColorGroup();

		Property rdfType = summary.getProperty(D3JSONGenerator.NS_RDF, D3JSONGenerator._type);
		StmtIterator si = summary.listStatements();

		while (si.hasNext()) {
			Statement s = si.nextStatement();
			Property p = s.getPredicate();
			if (p.getNameSpace().equals(D3JSONGenerator.NS_RDFS) || p.getNameSpace().equals(D3JSONGenerator.NS_OWL)) {
				// RDFS and OWL namespaces
				// XXX will deal with them when we add schema level info,
				// but that won't be in the NL diagram itself
				// can state the subClassOf relationships in nodes' tooltip
			} else if (p.getNameSpace().equals(D3JSONGenerator.NS_RDF)) {
				// RDF namespace
				if (p.getLocalName().equals(D3JSONGenerator._type)) {
					Resource object = ((Resource) s.getObject());
					if (isIgnoredNameSpace(object.getNameSpace())) {
						// likely a schema-level statement, declaring a Class or
						// Property
						// ignore it
						continue;
					}
					// add class info for nodes
					String sURI = s.getSubject().getURI();
					Node node = new Node(sURI);
					if (!nodes.contains(node))
						nodes.add(node);
					node.addClass(object.getLocalName());
					// color node according to its class' namespace
					node.addColorGroup(getColorGroup(((Resource) s.getObject()).getNameSpace()));
				}
			} else {
				// any other property
				String sURI = s.getSubject().getURI();
				String pURI = p.getURI();

				Node sNode = new Node(sURI);
				if (!nodes.contains(sNode))
					nodes.add(sNode);
				int sIndex = nodes.indexOf(sNode);

				Resource object = ((Resource) s.getObject());
				String oURI = object.getURI();
				int oIndex;

				if (rdfType != null && object.hasProperty(rdfType)) {
					// if object has rdf:type info, then seek possible prior
					// declaration of that node and use it
					Node oNode = new Node(oURI);
					if (!nodes.contains(oNode))
						nodes.add(oNode);
					oIndex = nodes.indexOf(oNode);

				} else {
					// if not, then regardeless of whether this is a single node
					// in the summary or not,
					// treat the summary statement's object as a unique node as
					// many props
					// can end up on the same node even if their property's
					// semantics (and thus range)
					// are widely different (making it unlikely that they would
					// point to the same kind
					// of resource)

					Node oNode = new Node(oURI, pURI);
					nodes.add(oNode);
					oIndex = nodes.indexOf(oNode);
				}
				links.add(new Link(pURI, sIndex, oIndex, getColorGroup(p.getNameSpace()), p.getLocalName()));
			}
		}
		// XXX reconstruct subclass / subproperty+domain+range info from what's
		// been parsed
		// and add that schema data to the nodes and props where relevant
		summary.close();
	}

	private static boolean isIgnoredNameSpace(String ns) {
		for (String ins : IGNORED_NAMESPACES) {
			if (ns.equals(ins)) {
				return true;
			}
		}
		return false;
	}

	private int getColorGroup(String ns) {
		if (ns2colorgroup.containsKey(ns)) {
			return ns2colorgroup.get(ns).intValue();
		} else {
			// ignored namespace (in terms of coloring)
			return -1;
		}
	}

	public String getFdJson() {

		return "";
	}

	public String getHebJeson() {
		return "";
	}

	private class Node {

		String uri;
		String p_uri;
		String[] classURIs;
		String label = D3JSONGenerator.EMPTY_STRING;
		int[] colorGroups;

		public Node(String uri) {
			this(uri, null);
		}

		public Node(String uri, String pURI) {
			this.uri = uri;
			this.p_uri = pURI;
		}

		public void addClass(String cURI) {
			if (classURIs == null) {
				classURIs = new String[] { cURI };
			} else {
				boolean alreadyDeclared = false;
				for (int i = 0; i < classURIs.length; i++) {
					if (classURIs[i].equals(cURI)) {
						alreadyDeclared = true;
						break;
					}
				}
				if (!alreadyDeclared) {
					String[] ncuris = new String[classURIs.length + 1];
					System.arraycopy(classURIs, 0, ncuris, 0, classURIs.length);
					ncuris[classURIs.length] = cURI;
					classURIs = ncuris;
				}
				Arrays.sort(classURIs);
			}
		}

		public void addColorGroup(int cg) {
			if (colorGroups == null) {
				colorGroups = new int[] { cg };
			} else {
				boolean alreadyDeclared = false;
				for (int i = 0; i < colorGroups.length; i++) {
					if (colorGroups[i] == cg) {
						alreadyDeclared = true;
						break;
					}
				}
				if (!alreadyDeclared) {
					int[] ncgs = new int[colorGroups.length + 1];
					System.arraycopy(colorGroups, 0, ncgs, 0, colorGroups.length);
					ncgs[colorGroups.length] = cg;
					colorGroups = ncgs;
				}
				Arrays.sort(colorGroups);
			}
		}

		public String[] getLabels() {
			if (classURIs == null) {
				return new String[0];
			} else {
				return classURIs;
			}
		}

		public int[] getColorGroups() {
			if (colorGroups == null) {
				return new int[0];
			} else {
				return colorGroups;
			}
		}
	}

	private class Link {

		private final String uri;
		private final String label;
		private int group;
		private final int subjectIdx, objectIdx;
		// will be null if namespace not found at init time
		private final int colorGroup;

		public Link(String uri, int sIndex, int oIndex, int cg, String label) {
			assert (uri != null && !"".equals(uri) && label != null);
			this.uri = uri;
			this.label = label;
			this.subjectIdx = sIndex;
			this.objectIdx = oIndex;
			this.colorGroup = cg;
		}

		/**
		 * @return the uri
		 */
		public String getUri() {
			return uri;
		}

		/**
		 * @return the label
		 */
		public String getLabel() {
			return label;
		}

		/**
		 * @return the group
		 */
		public int getGroup() {
			return group;
		}

		/**
		 * @return the subjectIdx
		 */
		public int getSubjectIdx() {
			return subjectIdx;
		}

		/**
		 * @return the objectIdx
		 */
		public int getObjectIdx() {
			return objectIdx;
		}

		/**
		 * @return the colorGroup
		 */
		public int getColorGroup() {
			return colorGroup;
		}

	}

}
