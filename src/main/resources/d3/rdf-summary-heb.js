/*
 * LODSummaries
 * Copyright (C) 2018  INRIA Emmanuel Pietriga <emmanuel.pietriga@inria.fr>
 * Copyright (C) 2018  INRIA Marie Destandau <marie.destandau@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

var hebCtx = {
    width: 960, // adapt to actual SVG size
    height: 960,
    PROP_LABEL_MAX_LENGTH: 10,
    NS_ARC_THICKNESS: 14,
    TYPE_ARC_THICKNESS: 14,
    highlightColor: "#d78383", // red-ish
    color: d3.scale.category20(),
    noNScolor: "#CCC",
    NO_NS: "nns", // no namespace
    NO_TYPE: "ntp",
    LETTER_SIZE: 2,// space needed by each letter to display the name of a group of classes
    MIN_GROUP_SIZE: 10,
    SEARCH_ACTIVE: false, // flag to deactivate keyboard mapping when using search input
    mainGroup: null,
    defs: null,
    nodes:null,
    links:null,
    tree: null,
    propList:null,
    rotate:0,
    panning:false,
    rotating:false,
    translate:[0,0],
    scale:1,
    protate:0,
};
var initHebCtx = hebCtx;

var hebConsts = {
    MODE_PAN: 0,
    MODE_ROTATE: 1,
};

/* -------------------------------------------------------------------------- */

var buildHEB = function(graph){

    var cluster = d3.layout.cluster()
        .size([360, hebCtx.innerRadius])
        .sort(null)
        .value(function(d) { return d.size; });

    var bundle = d3.layout.bundle();

    var line = d3.svg.line.radial()
        .interpolate("bundle")
        .tension(.85)
        .radius(function(d) { return d.y; })
        .angle(function(d) { return d.x / 180 * Math.PI; });

    // containers for groups of svg objects
    hebCtx.mainGroup.append("g").attr("id", "linkSplinesG");
    hebCtx.mainGroup.append("g").attr("id", "linkLabelsG");
    hebCtx.mainGroup.append("g").attr("id", "linkArcsG");
    hebCtx.mainGroup.append("g").attr("id", "typeArcsG");
    hebCtx.mainGroup.append("g").attr("id", "typeLabelsG");

    // references to the groups
    var linkSplines = d3.select("#linkSplinesG").selectAll("path.link"),
        linkLabels = d3.select("#linkLabelsG").selectAll("text.node"),
        linkArcs = d3.select("#linkArcsG").selectAll("path");

    // init tree
    hebCtx.tree = {name:""};
    hebCtx.tree.children = []
    // first level in the hierarchy holds groups of classes
    // second level holds leaves
    // leaves are also stored in a flat array
    var groupIndexes = {}
    for (var i=0; i<graph['nodes'].length; i++){
        var n = graph['nodes'][i];

        // named class or combination of named classes
        if (n.labels.length > 0){
            graph['nodes'][i].name = n.labels.join();
        }else {
            // unidentified class
            graph['nodes'][i].name = "?";
        }
        var newGroupNode = { name: graph['nodes'][i].name, parent:hebCtx.tree, children: [], groups: n.groups, indexes: [i]};
        //if the group does not already exist, add it to the tree
        var checkNode = null;
        hebCtx.tree.children.forEach(function(element, index){
            if (element.name === newGroupNode.name )  checkNode = index;
        })
        if(checkNode !== null){
            hebCtx.tree.children[checkNode].indexes.push(i);
        }else{
            hebCtx.tree.children.push(newGroupNode);
            checkNode = hebCtx.tree.children.length - 1;
        }
        groupIndexes[newGroupNode.name] = checkNode;
    }

    // walk all links to build third level in the hierarchy (leaf nodes), holding property cells
    // which are considered as the actual nodes for the visualized graph
    // and also populate the list of sources/targets
    // - init list of all edges to be bundled and laid out, linking leaf nodes
    var rslinks = [];
    hebCtx.label2group = {};
    // for each link
    for (var i = 0; i < graph['links'].length; i++){
        var prop = graph['links'][i];
        var sourceNodeGroupIndex = groupIndexes[graph['nodes'][prop.source].name];
        var targetNodeGroupIndex = groupIndexes[graph['nodes'][prop.target].name];

        // check if already exists
        var checkSource = null;
        var checkTarget = null;
        hebCtx.tree.children[sourceNodeGroupIndex].children.forEach(function(element, index){
            if(element.name === prop.label) checkSource = index;
        })
        hebCtx.tree.children[targetNodeGroupIndex].children.forEach(function(element, index){
            if(element.name === prop.label) checkTarget = index;
        })

        // create a leaf & adds it in the groups of the source node if it does not exist
        var sourceLeaf ;
        if(checkSource !== null){
            sourceLeaf = hebCtx.tree.children[sourceNodeGroupIndex].children[checkSource]
        } else {
            sourceLeaf = { name: prop.label, rdfslabel: prop.rdfslabel || prop.label, key:prop.label, parentIndex: sourceNodeGroupIndex }
            hebCtx.tree.children[sourceNodeGroupIndex].children.push(sourceLeaf);
        }
        // create a leaf & adds it in the groups of the target node if it does not exist
        var targetLeaf ;
        if(checkTarget !== null && checkSource !== checkTarget){
            targetLeaf = hebCtx.tree.children[targetNodeGroupIndex].children[checkTarget]
        } else {
            targetLeaf = { name: prop.label, rdfslabel: prop.rdfslabel || prop.label, key:prop.label, parentIndex: targetNodeGroupIndex }
            hebCtx.tree.children[targetNodeGroupIndex].children.push(targetLeaf);
        }
        rslinks.push({ source: sourceLeaf, target: targetLeaf });
        hebCtx.label2group[prop.label] = prop.group;
    }
    // rslinks = list of objects representing links, with 2 properties : source and target
    // hebCtx.label2group  = object indicating the number of links for each label (ex { livesIn: 4, bornIn: 2 })

    hebCtx.tree.children = hebCtx.tree.children.map(function(group, indexGroup){
        group.children = group.children.map(function(node){
            node.group = hebCtx.label2group[node.name]
            return node;
        }).sort(function(a, b){
            if (a.group == b.group) {
                // console.log(a, b)
                // console.log(a, b, a.name.localeCompare(b.name))
                return a.name.localeCompare(b.name);
            }
            return a.group - b.group;
        });
        return group;
    });

    // edge bundling hierarchy
    //
    // make a cluster on full tree
    var rsnodes = cluster.nodes(hebCtx.tree);
    // then keep the leaves to draw outer arcs and labels first
     var leafNodes = rsnodes.filter(function(n) { return !n.children && n.depth == 2; });

    hebCtx.links = linkSplines.data(bundle(rslinks))
        .enter().append("path")
        .each(function(d) { d.source = d[0], d.target = d[d.length - 1]; })
        .attr("class", "link")
        .attr("d", line)
        .style("stroke", function(d){
            return hebCtx.color(hebCtx.label2group[d.source.name]);
        });

    //leafNodes = leafNodes.map(function(d){ d.group = hebCtx.label2group[d.name]; return d; })
    //leafNodes = leafNodes.sort(function(a,b){ return a.group - b.group; })

    hebCtx.linkLabels = linkLabels.data(leafNodes)
        .enter().append("text")
        .attr("class", "node")
        .attr("dy", ".31em")
        .attr("transform", function(d) {
            //console.log(d);
            return "rotate(" + (d.x - 90) + ")translate(" + (d.y + hebCtx.NS_ARC_THICKNESS + 4 + hebCtx.TYPE_ARC_THICKNESS + 4) + ",0)" + (d.x < 180 ? "" : "rotate(180)"); })
        .style("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
        .text(function(d) { return (d.key.length > hebCtx.PROP_LABEL_MAX_LENGTH) ? d.key.substring(0, hebCtx.PROP_LABEL_MAX_LENGTH) + "..." : d.key; })
        .on("mouseover", function(d) { mouseEnteredLinkLabel(d.name) })
        .on("mouseout", function(d) { mouseExitedLinkLabel(d.name) });


    // https://github.com/mbostock/d3/wiki/SVG-Shapes#arc

    // d3 positions the leaves, and then we will use their position to calculate the position of the group of classes
    var propArc = d3.svg.arc()
        .innerRadius(hebCtx.innerRadius+hebCtx.NS_ARC_THICKNESS+4)
        .outerRadius(hebCtx.innerRadius+hebCtx.NS_ARC_THICKNESS+4+hebCtx.TYPE_ARC_THICKNESS);

    linkArcs.data(leafNodes)
        .enter().append("path")
        .attr("d", function(d,i){propArc.startAngle(d.x*Math.PI/180).endAngle(d.x*Math.PI/180+0.02);return propArc();})
        .attr("stroke", "#444")
        .style("fill", function(d){
            return hebCtx.color(hebCtx.label2group[d.name]);
        });


    // groups of classes sharing the same links
    typedNodeAngleSpansT = {};
    // checks each leave
    for (var i=0;i<leafNodes.length;i++){
        // if there's already an entry for its group
        if (typedNodeAngleSpansT.hasOwnProperty(leafNodes[i].parent.name)){
            var angleInfo = typedNodeAngleSpansT[leafNodes[i].parent.name];
            // if the current leave appears before the stored value for min Angle, replace the value
            if (angleInfo.minAngleDeg > leafNodes[i].x){
                angleInfo.minAngleDeg = leafNodes[i].x;
            }
            // if the current leave appears after the stored value for max Angle, replace the value
            if (angleInfo.maxAngleDeg < leafNodes[i].x){
                angleInfo.maxAngleDeg = leafNodes[i].x +1;
            }
        }
        // if there's no entry for this group, create it with current leave
        else {
            var angleInfo = {minAngleDeg: leafNodes[i].x, maxAngleDeg: leafNodes[i].x, name: leafNodes[i].parent.name};
            typedNodeAngleSpansT[leafNodes[i].parent.name] = angleInfo;
        }
    }

    // typedNodeAngleSpansT = object with a property for each group of classes,
    // containing an object defining angle properties
    // ex : { Book: {minAngleDeg: 1, maxAngleDeg : 2, name : "Book"}, ... }


    // calculate angles for groups of classes
    var typedNodeAngleSpans = [];
    for (var k in typedNodeAngleSpansT){
        var tnas = typedNodeAngleSpansT[k];
        if ((tnas.maxAngleDeg-tnas.minAngleDeg) < 2){
            tnas.minAngleDeg -= 1;
            tnas.maxAngleDeg += 1;
        }
        typedNodeAngleSpans.push(tnas);
    }
    // typedNodeAngleSpans = list of objects defining angle properties for each group of classes
    // ex : [ {minAngleDeg: 1, maxAngleDeg : 2, name : "Book"}, ... ]
    // (same infos as in typedNodeAngleSpansT in a list format)

    // check if each group has enough space to display its name
    hebCtx.typedNodeAngleSpans = typedNodeAngleSpans.map(function(element, i){
        var angleSpan = getAngleSpan(element.minAngleDeg, element.maxAngleDeg);
        var extraAngleNeeded = (hebCtx.MIN_GROUP_SIZE + hebCtx.LETTER_SIZE * element.name.length) - angleSpan;
        // if it's not the case
        if (extraAngleNeeded > 0){
            var previousGroup = (i > 0) ? (i - 1) : (typedNodeAngleSpans.length - 1);
            var nextGroup = (i < typedNodeAngleSpans.length - 1) ? (i + 1) : 0;
            var spaceBefore = getAngleSpan(typedNodeAngleSpans[previousGroup].maxAngleDeg, element.minAngleDeg) - 10; // keep a minimum space
            var spaceAfter = getAngleSpan(element.maxAngleDeg, typedNodeAngleSpans[nextGroup].minAngleDeg) - 10; // of 10 degrees
            // if there's space left, make the angle bigger
            if(spaceBefore > 0 && spaceAfter > 0){
                // if space available bigger than space needed, add space needed
                // else add space available (missing space info will be stored for rollover)
                var addBefore = spaceBefore > (extraAngleNeeded/2) ?  (extraAngleNeeded/2) : spaceBefore;
                var addAfter = spaceAfter > (extraAngleNeeded/2) ?  (extraAngleNeeded/2) : spaceAfter;
                //console.log('addBefore', addBefore, 'addAfter', addAfter,  element.minAngleDeg,  element.maxAngleDeg);
                element.minAngleDeg -= addBefore;
                element.maxAngleDeg += addAfter;
                // substracts what has been added from what is needed
                extraAngleNeeded -= (addBefore + addAfter);
            }
        }
        // saves info to be used on rollover
        if(extraAngleNeeded > 0){
            element.extraAngleNeeded = extraAngleNeeded;
            element._minAngleDeg = element.minAngleDeg - element.extraAngleNeeded / 2;
            element._maxAngleDeg = element.maxAngleDeg + element.extraAngleNeeded / 2;
        }else{
            element.extraAngleNeeded = 0;
            element._minAngleDeg = element.minAngleDeg;
            element._maxAngleDeg = element.maxAngleDeg;
        }
        return element;
    })

    hebCtx.nsArc = d3.svg.arc()
        .innerRadius(hebCtx.innerRadius-2)
        .outerRadius(hebCtx.innerRadius+hebCtx.NS_ARC_THICKNESS);

    var arcs = [];


    var drawArc = function(d, over){
        var minAngle = over ? d._minAngleDeg : d.minAngleDeg;
        var maxAngle = over ? d._maxAngleDeg : d.maxAngleDeg;
        hebCtx.nsArc.startAngle(minAngle *Math.PI/180)
            .endAngle(maxAngle *Math.PI/180);
        var a = hebCtx.nsArc();
        return a;
    }
    var classGroup = d3.select("#typeArcsG").selectAll("g")
        .data(hebCtx.typedNodeAngleSpans)
        .enter().append("g")
        .attr('id', function(d){ return 'class_group_' + formatName(d.name) })
        .on("mouseover", function(d){
            adjustArcsG(formatName(d.name), true);
        })
        .on("mouseleave", function(d){
            adjustArcsG(formatName(d.name), false);
        });

    classGroup.append("path")
        .attr("d", function(d,i){
            return drawArc(d, false);
        })
        .attr("stroke", "#444")
        .attr("fill", "#DDD")

    //console.log('arcs ',arcs);

    // http://bl.ocks.org/jebeck/196406a3486985d2b92e
    // typed node info as textPaths
    for (var i=0;i<hebCtx.typedNodeAngleSpans.length;i++){
        hebCtx.defs.append("path").attr({
            "id": formatName(typedNodeAngleSpans[i].name),
            "d": generateArcPath(hebCtx.typedNodeAngleSpans[i].minAngleDeg, hebCtx.typedNodeAngleSpans[i].maxAngleDeg, hebCtx.innerRadius*1.01)
        });
        hebCtx.defs.append("path").attr({
            "id": "full_" + formatName(typedNodeAngleSpans[i].name),
            "d": generateArcPath(hebCtx.typedNodeAngleSpans[i]._minAngleDeg, hebCtx.typedNodeAngleSpans[i]._maxAngleDeg, hebCtx.innerRadius*1.01)
        });
    }
    classGroup.append("text").append("textPath")
        .attr("startOffset", '50%')
        .style("text-anchor", "middle")
        .attr('id', function(d){ return 'class_group_text_' + formatName(d.name) })
        .attr("xlink:href", function(d,i){return "#"+formatName(d.name);})
        .text(function(d){return d.name});


        var adjustArcsG = function(name, over){

            d3.selectAll('#typeArcsG g')
                .each(function(d){ d.active = (formatName(d.name) === name)? 1 : 0 })
                .sort(function(a,b){
                    return a.active - b.active;
                })
                .attr("z-index", function(d){ return d.active; })
                .attr("render-order", function(d){ return d.active; });

            d3.selectAll('#class_group_' + name + ' path')
                .attr("d", function(d){
                    return drawArc(d, over)
                })
                .attr("fill", over ? "#444" : "#ddd");
                //force the browser to redraw

            d3.selectAll('#class_group_' + name + ' text')
                .attr("fill", over ? "#ddd" : "#444");

            d3.selectAll('#class_group_' + name + ' textPath')
                .attr("xlink:href", function(d, i){
                    return over ? "#full_"+formatName(d.name) : "#"+formatName(d.name);
                })
                //force the browser to redraw
                .attr("transform", over ? "scale(1.05)" : "scale(1)");

        }


};

var getAngleSpan = function (minAngle, maxAngle) {
    minAngle = Number(minAngle);
    maxAngle = Number(maxAngle);
    if(maxAngle < minAngle) maxAngle += 360;
    return (maxAngle - minAngle);
}

// get the index of a label in a list
var getLeafNodeIndex = function(propLabel, leafNodeList){
    for (var i = 0; i < leafNodeList.length; i++){
        if (leafNodeList[i].name == propLabel){
            return i;
        }
    }
    return -1;
}

var generateArcPath = function(minAngleDeg, maxAngleDeg, radius){
    var spx = radius * Math.cos((minAngleDeg-90)*Math.PI/180);
    var spy = radius * Math.sin((minAngleDeg-90)*Math.PI/180);
    var epx = radius * Math.cos((maxAngleDeg-90)*Math.PI/180);
    var epy = radius * Math.sin((maxAngleDeg-90)*Math.PI/180);
    var res = "M" + spx + "," + spy;
    res += " A" + radius+","+radius;
    res += ((maxAngleDeg-minAngleDeg) <= 180) ? " 0 0,1 " : " 0 1,1 ";
    res += epx + "," + epy;
    return res;
};


//generates a group per namespace + 1 for no namespace (nns)
var generateGroupLevel = function(groups, root){
    var res = [];
    for (var i=0;i<groups.length;i++){
        res.push({ "name":groups[i], "group": i, children:[] });
    }
    // additional group for untyped placeholders
    var nt = { "name":hebCtx.NO_NS, "group": res.length, children:[] };
    nt.children.push({ "name": hebCtx.NO_TYPE, parent: nt, children:[] });
    res.push(nt);
    return res;
};

/* -------------------------------------------------------------------------- */
// HOVER
/* -------------------------------------------------------------------------- */
function mouseEnteredLinkLabel(propertyName) {
    hebCtx.propList.forEach(function(prop){
        if(propertyName == prop.rdfslabel) propertyName = prop.label;
    });
    // reset all link label colors
    hebCtx.linkLabels
        .each(function(n) { n.target = n.source = false; });

    // color hovered links
    hebCtx.links
        .classed("link--target", function(l) { if (l.target.name === propertyName) return l.source.source = true; })
        .classed("link--source", function(l) { if (l.source.name === propertyName) return l.target.target = true; })
        .filter(function(l) { return l.target.name === propertyName || l.source.name === propertyName; })
        .each(function() { this.parentNode.appendChild(this); });

    // color hovered link labels
    hebCtx.linkLabels
        .classed("node--target", function(n) { return n.target; })
        .classed("node--source", function(n) { return n.source; });

    // expand name of hovered link labels
    d3.selectAll("text.node--target").text(
        function(d2){ return d2.key; }
    );
    d3.selectAll("text.node--source").text(
        function(d2){ return d2.key; }
    );
    d3.selectAll(".proplistitem")
        .classed("highlighted", function(p) { if (p.label === propertyName) return true; });
    d3.selectAll("#propListPane .highlighted")
        .style("background-color", hebCtx.highlightColor)

};

function mouseExitedLinkLabel() {
    hebCtx.links
        .classed("link--target", false)
        .classed("link--source", false);

    hebCtx.linkLabels
        .classed("node--target", false)
        .classed("node--source", false);

    d3.selectAll(".proplistitem ")
        .classed("highlighted", false)
        .attr("style", null)

    adjustPropLabelLength();

//console.log("INPUT",$('#searchProp').val())
    if( $('#searchProp').val() !== ""){
        mouseEnteredLinkLabel($('#searchProp').val());
    }
};


/* -------------------------------------------------------------------------- */
// PROPERTY LIST
/* -------------------------------------------------------------------------- */

var buildPropertyList = function(graph){
    //console.log(".///////////////////",graph)
    d3.select("#propListPane").on("mouseleave", function(d){
        mouseExitedLinkLabel();
    });

    // extract list of unique prop URIs and bind it to the list items

    var propURIs = [];
    var propColorGroup = [];
    var rdfslabels = [];
    for (var i=0;i<graph.links.length;i++){
        // if not already extracted
        if (propURIs.indexOf(graph.links[i].label) == -1){
            // add to the list
            propURIs.push(graph.links[i].label);
            propColorGroup.push(graph.links[i].group);
            rdfslabels.push(graph.links[i].rdfslabel||graph.links[i].label);
        }
    }

    hebCtx.propList = [];
    for (var i=0;i<propURIs.length;i++){
        hebCtx.propList.push({label:propURIs[i], group:propColorGroup[i], rdfslabel:rdfslabels[i]});
    }

    var propComp = function(a,b){
        // sort by namespace, then alphabetically
        if (a.group < b.group){return -1;}
        else if (a.group > b.group){return 1;}
        else {
            if (a.label < b.label){return -1;}
            else if (a.label > b.label){return 1;}
            else {
                return 0;
            }
        }
    }
    hebCtx.propList.sort(propComp);

    var pldiv = d3.select("#propListPane").selectAll("div")
        .data(hebCtx.propList)
        .enter().append("div")
        .attr("class", function(d,i){
            if (i % 2 == 0){
                return "proplistitem even";
            }
            else {
                return "proplistitem odd";
            }
        })
        .on("mouseover", function(d){
            mouseEnteredLinkLabel(d.label)
        })
        .on("mouseleave", function(d){
            mouseExitedLinkLabel(d.label)
        });

    pldiv.append("div")
         .attr("class", "legend")
         .style("background-color", function(d){return hebCtx.color(d.group)});

    pldiv.append("div").text(function(d){return d.rdfslabel;});

    //TYPEAHEAD
    //autocomplete search form
    //https://github.com/twitter/typeahead.js/blob/master/doc/jquery_typeahead.md
    var substringMatcher = function(strs) {
      return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
              if (substrRegex.test(str)) {
                matches.push(str);
              }
            });

            cb(matches);
        };
    };
    $('#autocomplete').append("<input type='text' id='searchProp' />");
    $('#searchProp').typeahead(null, {
      name: 'properties',
      limit: 15,
      source: substringMatcher(hebCtx.propList.map(function(element){ return element.rdfslabel } ))
    });
    $('#searchProp').bind('typeahead:cursorchange', function(e, suggestion) {
        mouseExitedLinkLabel()
        mouseEnteredLinkLabel(suggestion)
    });
    $('#searchProp').bind('typeahead:active', function(e) {
        hebCtx.SEARCH_ACTIVE = true;
    });
    $('#searchProp').bind('typeahead:idle', function(e) {
        hebCtx.SEARCH_ACTIVE = false;
        mouseExitedLinkLabel()
    });
    $('#searchProp').bind('typeahead:autocomplete', function(e, suggestion) {
        mouseExitedLinkLabel()
        mouseEnteredLinkLabel(suggestion)
    });

};

/* -------------------------------------------------------------------------- */
// Visualization params adjustments
/* -------------------------------------------------------------------------- */



var formatName = function(name){
    name = name.replace('?', 'unknown');
    name = name.replace(/[,0-9\s]*/mg,"");
    return name;
}

var adjustPropLabelLength = function(keyEvent){
    if(keyEvent){
        hebCtx.PROP_LABEL_MAX_LENGTH += (keyEvent.shiftKey) ? -1 : 1;
    }
    if (hebCtx.PROP_LABEL_MAX_LENGTH < 0){
        hebCtx.PROP_LABEL_MAX_LENGTH = 0;
    }
    d3.selectAll("text.node").text(
        function(d) {
            return getTrimmedPropLabel(d.key);
        }
    );
}

var getTrimmedPropLabel = function(propLb){
    return (propLb.length > hebCtx.PROP_LABEL_MAX_LENGTH) ? propLb.substring(0, hebCtx.PROP_LABEL_MAX_LENGTH) + "..." : propLb;
};


/* -------------------------------------------------------------------------- */
// INITIALIZE CONTROLS FOR ROTATION, PAN, ZOOM & LABELS...
/* -------------------------------------------------------------------------- */

var initViz = function(){

    var zoom = d3.behavior.zoom()
                 .scaleExtent([1, 10])
                 .on("zoom", zoomed);

    var diameter = hebCtx.height ,
        radius = diameter / 3;
    hebCtx.innerRadius = radius - 120;

    hebCtx.rootTranslate = 1.2*radius;

    var svg = d3.select("#heb").append("svg")
        .attr("width", "100%")
        .attr("height", diameter)

        var rect = svg.append("rect")
                      .attr("width", d3.selectAll("svg").attr("width"))
                      .attr("height", d3.selectAll("svg").attr("height"))
                      .style("fill", "none")
                      .style("pointer-events", "all").call(zoom);

    var svgmg = svg.append("g")
                   .attr("transform", "translate(" + hebCtx.rootTranslate + "," + hebCtx.rootTranslate + ")");

    hebCtx.defs = d3.select("svg").append("defs");
    hebCtx.mainGroup = svgmg.append("g");
    hebCtx.svg = svgmg;

    d3.select("body")
        .on("keydown", function() {
            if(!hebCtx.SEARCH_ACTIVE) {
                if (d3.event.keyCode == 32){
                    // pressing space bar
                    if (!d3.event.repeat){
                        toggleMode(hebConsts.MODE_PAN);
                    }
                }
                else if (d3.event.code == "KeyR"){
                    if (!d3.event.repeat){
                        toggleMode(hebConsts.MODE_ROTATE);
                    }
                }
                else if (d3.event.code == "KeyT"){
                    adjustPropLabelLength(d3.event);
                }
            }
        })
        .on("keyup", function() {});
};


var toggleMode = function(mode){
    if (mode == hebConsts.MODE_PAN){
        if (hebCtx.panning){
            hebCtx.panning = false;
            d3.select("#status").text("");
        }
        else {
            hebCtx.panning = true;
            hebCtx.rotating = false;
            d3.select("#status").text("Drag to pan, mouse wheel to zoom");
        }
    }
    else if (mode == hebConsts.MODE_ROTATE){
        if (hebCtx.rotating){
            hebCtx.rotating = false;
            d3.select("#status").text("");
        }
        else {
            hebCtx.rotating = true;
            hebCtx.panning = false;
            d3.select("#status").text("Drag to rotate");
        }
    }
};

/* -------------------------------------------------------------------------- */

var zoomed = function zoomed() {
    if (hebCtx.panning){
        hebCtx.translate = d3.event.translate;
        hebCtx.scale = d3.event.scale;
    }
    else if (hebCtx.rotating){
        // inverse horizontal drag direction / rotation direction mapping
        // depending on whether cursor is in upper or lower part of the visualization
        // (provides stronger sense of direct manipulation given the circular nature
        //  of this visualization)
        var upperSide = (d3.event.sourceEvent.y <= hebCtx.rootTranslate) ? -1 : 1;
        hebCtx.rotate += ((hebCtx.protate - d3.event.translate[0]) > 0) ? upperSide * 4 : -4 * upperSide;
        hebCtx.protate = d3.event.translate[0];
    }
    hebCtx.mainGroup.attr("transform", "translate(" + hebCtx.translate + ")scale(" + hebCtx.scale + ")rotate("+hebCtx.rotate+")");
}

/* -------------------------------------------------------------------------- */
// SELECT MENU FOR JSON FILES
/* -------------------------------------------------------------------------- */

var jsonList = [
    { path: 'data/heb-clinical.json', label : 'Clinical' },
    { path: 'data/heb-dblp.json' },
    { path: 'data/heb-dbpedia_person.json' },
    { path: 'data/heb-firebrigade.json' },
    { path: 'data/heb-ilda-test.json' },
    { path: 'data/heb-ilda-test2.json' },
    { path: 'data/heb-insee.json' },
    { path: 'data/heb-lubm1m.json' },
    { path: 'data/heb-peel.json' },
    { path: 'data/heb-summary.json' }
];

var buildSelect = function(list){
    d3.select('#jsonSelect')
        .append('form')
        .append('select')
        .selectAll("option")
        .data(list)
        .enter()
        .append('option')
        .attr('value', function(d){ return d.path })
        .text( function(d){ return d.label || d.path });

    d3.select('#jsonSelect select')
        .on("change", function(e){
            loadData(d3.select('#jsonSelect select').property('value'));
    } )

}

/* -------------------------------------------------------------------------- */
// LOADING & INIT
/* -------------------------------------------------------------------------- */

var loadData = function(pathFile){
    d3.json(pathFile, function(error, graph) {
      if (error) throw error;
      // clean previous viz
      resetViz();
      // build new viz
      initViz();
      buildHEB(graph);
      buildPropertyList(graph);
    });
};


var resetViz = function(){
    hebCtx = initHebCtx;
    d3.select('#heb svg').remove()
    d3.selectAll('.proplistitem').remove()
    d3.select('#autocomplete *').remove()
};


document.addEventListener('DOMContentLoaded', function(event) {
    buildSelect(jsonList);
    loadData(jsonList[4].path);
})

