/*
 * LODSummaries
 * Copyright (C) 2018  INRIA Emmanuel Pietriga <emmanuel.pietriga@inria.fr>
 * Copyright (C) 2018  INRIA Marie Destandau <marie.destandau@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

var fdCtx = {
    width: 800, // adapt to actual SVG size
    height: 800,
    color: d3.scale.category20(),
    highlightColor: "#d78383", // red-ish
    noNScolor: "#CCC",
    mainGroup: null,
    dragFct: null,
    force: null,
    panning: false, // mode (triggered by space bar)
    nodeRadius: 6,
};

var initFdCtx = fdCtx;

/* -------------------------------------------------------------------------- */

var buildViz = function(graph){

    // https://github.com/mbostock/d3/wiki/Force-Layout
    var force = d3.layout.force()
                  .size([fdCtx.width, fdCtx.height/2])
                  .linkStrength(0.1)
                  .friction(0.9)
                  .linkDistance(20)
                  .charge(-30)
                  .gravity(0.1)
                  .theta(0.8)
                  .alpha(0.1);

    force.nodes(graph.nodes)
         .links(graph.links)
         .start();

    fdCtx.force = force;

    var linkG = fdCtx.mainGroup.append("g")
                               .attr("id", "links");

    var link = linkG.selectAll(".link")
        .data(graph.links)
        .enter().append("line")
        .attr("class", "link")
        .style("stroke", function(d) {
            if (d.group == -1){return fdCtx.noNScolor;}
            else {return fdCtx.color(d.group);}
        })
        .on("mouseover", function(d){
            highlightPropInstances(graph, d.label);
            highlightPropListItem(d.label);
        })
        .on("mouseleave", function(d){
            resetPropListHighlighting();
            resetPropInstanceHighlighting();
        });

    var nodeShG = fdCtx.mainGroup.append("g")
                                 .attr("id", "nodeShs");

    var nodeLbG = fdCtx.mainGroup.append("g")
                                 .attr("id", "nodeLbs");

    var nodeG = nodeShG.selectAll("g.node")
                       .data(graph.nodes)
                       .enter().append("g")
                       .attr("class", "node")
                       .call(force.drag);

    // for now we are color the node according to the first class we find. Need to find a representation
    // that depicts all the namespaces (if more than one class, and those classes belong to different namespaces)
    nodeG.append("circle")
        .attr("r", fdCtx.nodeRadius)
        .style("fill", function(d) {
            if (d.groups.length == 0){return fdCtx.noNScolor;}
            else {return fdCtx.color(d.groups[0]);}
        })
        .on("mouseover", function(d){
            highlightPropListItems(graph, d);
            highlightNodeProps(graph, d);
            d3.select(this.parentNode).classed("highlight", true);
        })
        .on("mouseleave", function(d){
            resetPropListHighlighting();
            resetPropInstanceHighlighting();
            d3.select(this.parentNode).classed("highlight", false);
        });

    nodeG.append("title")
         .text(function(d) { return d.labels.join(); });

    var nodeLb = nodeLbG.selectAll("g.nodeLb")
                            .data(graph.nodes)
                            .enter().append("g")
                            .attr("class", "nodeLb")
                            .call(force.drag);

    nodeLb.append("text")
         .attr("transform", function(d) { return "translate("+(fdCtx.nodeRadius+1)+",0)"; })
         .text(function(d) { return d.labels.join(); });

    force.on("tick", function() {
      link.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });
      nodeG.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
      nodeLb.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
    });

};

/* -------------------------------------------------------------------------- */

var buildPropertyList = function(graph){

    d3.select("#propListPane").on("mouseleave", function(d){
        resetPropInstanceHighlighting();
    });

    // extract list of unique prop URIs and bind it to the list items
    var propURIs = [];
    var propColorGroup = [];
    for (var i=0;i<graph.links.length;i++){
        if (propURIs.indexOf(graph.links[i].label) == -1){
            propURIs.push(graph.links[i].label);
            propColorGroup.push(graph.links[i].group);
        }
    }

    var propList = [];
    for (var i=0;i<propURIs.length;i++){
        propList.push({label:propURIs[i], group:propColorGroup[i]});
    }

    var propComp = function(a,b){
        // sort by namespace, then alphabetically
        if (a.group < b.group){return -1;}
        else if (a.group > b.group){return 1;}
        else {
            if (a.label < b.label){return -1;}
            else if (a.label > b.label){return 1;}
            else {
                return 0;
            }
        }
    }
    propList.sort(propComp);

    var pldiv = d3.select("#propListPane").selectAll("div")
                  .data(propList)
                  .enter().append("div")
                  .attr("class", function(d,i){
                      if (i % 2 == 0){
                          return "proplistitem even";
                      }
                      else {
                          return "proplistitem odd";
                      }
                  })
                  .on("mouseover", function(d){
                      d3.select(this).style("background-color", fdCtx.highlightColor);
                      highlightPropInstances(graph, d.label);
                  })
                  .on("mouseleave", function(d){
                      resetPropInstanceHighlighting();
                      d3.select(this).attr("style", null);
                  });

    pldiv.append("div")
         .attr("class", "legend")
         .style("background-color", function(d){return fdCtx.color(d.group)});

    pldiv.append("div").text(function(d){return d.label;});

};

var resetPropInstanceHighlighting = function(){
    d3.select("svg").selectAll(".link")
      .attr("class", "link")
      .style("stroke", function(d) {if (d.group==-1){return "#000";} else {return fdCtx.color(d.group);} });
    d3.selectAll("g.node")
      .classed("highlight", false);
    d3.selectAll("g.nodeLb")
      .classed("highlight", false);
}

var highlightPropInstances = function(graph, propLocalName){
    // highlight link itself
    d3.select("svg").selectAll(".link")
        .data(graph.links)
        .attr("class", function(d){
            if (d.label == propLocalName){
                return "link highlight";
            }
            else {
                return "link";
            }
        })
        .style("stroke", function(d){
            if (d.label == propLocalName){
                return fdCtx.highlightColor;
            }
            else {
                if (d.group==-1){return "#000";}
                else {return fdCtx.color(d.group);}
            }
        });
    // highlight associated nodes (both source and target)
    d3.selectAll(".link.highlight")
      .each(function(d){
          d3.selectAll("g.node")
            .filter(function(d2){return d2 === d.source || d2 === d.target;})
            .classed("highlight", true);
          d3.selectAll("g.nodeLb")
            .filter(function(d2){return d2 === d.source || d2 === d.target;})
            .classed("highlight", true);
      });
};

var resetPropListHighlighting = function(){
    d3.select("#propListPane").selectAll("div.proplistitem")
      .style("background-color", null);
};

var highlightPropListItems = function(graph, node){
    var propertiesToHighlight = [];
    for (var i=0;i<graph.links.length;i++){
        if (graph.links[i].source.index == node.index ||
            graph.links[i].target.index == node.index){
            if (propertiesToHighlight.indexOf(graph.links[i].label) == -1){
                propertiesToHighlight.push(graph.links[i].label);
            }
        }
    }
    d3.select("#propListPane").selectAll("div.proplistitem")
      .style("background-color", function(d){
          if (propertiesToHighlight.indexOf(d.label) == -1){return null;}
          else {return fdCtx.highlightColor;}
      });
};

var highlightPropListItem = function(propLocalName){
    d3.select("#propListPane").selectAll("div.proplistitem")
      .style("background-color", function(d){
          if (d.label == propLocalName){return fdCtx.highlightColor;;}
          else {return null;}
      });
};

var highlightNodeProps = function(graph, node){
    d3.select("svg").selectAll(".link")
        .data(graph.links)
        .attr("class", function(d){
            if (d.source.index == node.index || d.target.index == node.index){
                return "link highlight";
            }
            else {
                return "link";
            }
        })
        .style("stroke", function(d){
            if (d.source.index == node.index || d.target.index == node.index){
                return fdCtx.highlightColor;
            }
            else {
                if (d.group==-1){return "#000";}
                else {return fdCtx.color(d.group);}
            }
        });
};

/* -------------------------------------------------------------------------- */

function zoomed() {
    if (fdCtx.panning){
        fdCtx.mainGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }
}

function dragstarted(d) {
    if (!fdCtx.panning){
        // d3.event.sourceEvent.stopPropagation();
        // //d3.event.sourceEvent.stopPropagation();
        // d3.select(this).style("stroke", "#111");
    }
}

function dragged(d) {
    if (!fdCtx.panning){
        // d3.select(this.parentNode).call(fdCtx.force.drag);
        // d3.select(this).attr("cx", d.x = d3.event.x).attr("cy", d.y = d3.event.y);
    }
}

function dragended(d) {
    if (!fdCtx.panning){
        // d3.select(this).style("stroke", "#fff");
    }
}

/* -------------------------------------------------------------------------- */

var initViz = function(){
    var zoom = d3.behavior.zoom()
                 .scaleExtent([1, 10])
                 .on("zoom", zoomed);

    fdCtx.dragFct = d3.behavior.drag()
                      .origin(function(d) { return d; })
                      .on("dragstart", dragstarted)
                      .on("drag", dragged)
                      .on("dragend", dragended);

    var svg = d3.select("#nld").append("svg")
                .attr("width", "100%")
                .attr("height", fdCtx.height)
                .append("g")
                .attr("transform", "translate(" + 0 + "," + 0 + ")")
                .call(zoom);

    var rect = svg.append("rect")
                  .attr("width", d3.selectAll("svg").attr("width"))
                  .attr("height", d3.selectAll("svg").attr("height"))
                  .style("fill", "none")
                  .style("pointer-events", "all");

    fdCtx.mainGroup = svg.append("g");

    d3.select("body")
        .on("keydown", function() {
                if(d3.event.keyCode == 32 && !d3.event.repeat){
                    // pressing space bar
                    fdCtx.panning = true;
                }
            })
        .on("keyup", function() {
                if(d3.event.keyCode == 32 && !d3.event.repeat){
                    // pressing space bar
                    fdCtx.panning = false;
                }
            });
};


/* -------------------------------------------------------------------------- */
// SELECT MENU FOR JSON FILES
/* -------------------------------------------------------------------------- */

var jsonList = [
    { path: 'data/fd-dblp.json', label : 'Force Directed 1' },
    { path: 'data/fd-dbpedia_person.json' },
    { path: 'data/fd-firebrigade.json' },
    { path: 'data/fd-ilda-test.json' },
    { path: 'data/fd-lubm1m.json' },
    { path: 'data/fd-summary.json' }
];

var buildSelect = function(list){
    d3.select('#jsonSelect')
        .append('form')
        .append('select')
        .selectAll("option")
        .data(list)
        .enter()
        .append('option')
        .attr('value', function(d){ return d.path })
        .text( function(d){ return d.label || d.path });

    d3.select('#jsonSelect select')
        .on("change", function(e){
            loadData(d3.select('#jsonSelect select').property('value'));
    } )

}
/* -------------------------------------------------------------------------- */
// LOADING & INIT
/* -------------------------------------------------------------------------- */

var loadData = function(pathFile){
    d3.json(pathFile, function(error, graph) {
      if (error) throw error;
      // clean previous viz
      resetViz();
      // build new viz
      initViz();
      buildViz(graph);
      buildPropertyList(graph);
    });
};

var resetViz = function(){
    fdCtx = initFdCtx;
    d3.select('#nld svg').remove()
    d3.selectAll('.proplistitem').remove()
};


document.addEventListener('DOMContentLoaded', function(event) {
    buildSelect(jsonList);

    loadData(jsonList[0].path);
})

